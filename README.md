# Check WebGL Platform

This repository contain example how you can check on which platform your WebGL build is running. You can check how it was created!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2020/10/05/check-webgl-platform/

Enjoy!

---

# How to use it?

Clone this repository to you computer or browse it online!

If you want to see that implementation, go straight to [Assets/WebGLTemplates/](https://bitbucket.org/gaello/check-webgl-platform/src/master/Assets/WebGLTemplates/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
